package com.example.contact

import android.Manifest
import android.content.pm.PackageManager
import android.database.Cursor
import android.os.Build
import android.os.Bundle
import android.provider.ContactsContract
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.loader.app.LoaderManager
import androidx.loader.content.CursorLoader
import androidx.loader.content.Loader
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.example.contact.adapter.ContactAdapter
import com.example.contact.data.db.Contact
import com.example.contact.databinding.ActivityMainBinding
import com.jakewharton.rxbinding.widget.RxTextView
import kotlinx.android.synthetic.main.activity_main.*
import rx.android.schedulers.AndroidSchedulers
import java.util.concurrent.TimeUnit


/**
 * Ref : Contacts DB Structure : https://medium.com/@kednaik/android-contacts-fetching-using-coroutines-aa0129bffdc4
 *
 */
class MainActivity : AppCompatActivity(), LoaderManager.LoaderCallbacks<Cursor> {

    private lateinit var binding: ActivityMainBinding
    private lateinit var viewModel: MainViewModel
    private lateinit var contactAdapter: ContactAdapter
    private var contactMap: MutableMap<Long?, Contact?> = mutableMapOf()
    private var start: Long = 0

    private val listener: (Contact?) -> Unit = {
        showAlertDialog(it)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        viewModel = ViewModelProvider(this).get(MainViewModel::class.java)

        if (checkPermission()) {
            init()
        }
    }

    private fun init() {
        start = System.currentTimeMillis()
        LoaderManager.getInstance(this).initLoader(0, null, this)

        contactAdapter = ContactAdapter(listener)
        rv_contacts.apply {
            layoutManager = LinearLayoutManager(this@MainActivity)
            adapter = contactAdapter
        }
        viewModel.getLiveDataQuery().observe(this, Observer {
            contactAdapter.setList(it)
            Log.d(TAG, "init: size ${it.size}")
        })
        RxTextView.textChanges(et_search)
            .observeOn(AndroidSchedulers.mainThread())
            .debounce(200, TimeUnit.MILLISECONDS)
            .subscribe {
                Log.d(TAG, "onCreate: $it")
                viewModel.setLiveQuery(it.toString())
            }

    }

    private val PERMISSION_CODE: Int = 101

    private fun checkPermission(): Boolean {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true
        }
        if (checkSelfPermission(Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED
            ||
            checkSelfPermission(Manifest.permission.WRITE_CONTACTS) != PackageManager.PERMISSION_GRANTED
        ) {
            requestPermissions(
                arrayOf(
                    Manifest.permission.WRITE_CONTACTS,
                    Manifest.permission.READ_CONTACTS
                ), PERMISSION_CODE
            )
            return false
        }
        return true
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            PERMISSION_CODE -> if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(
                    this@MainActivity,
                    "Permission has been granted.",
                    Toast.LENGTH_SHORT
                ).show()
                init()
            } else {
                Toast.makeText(
                    this@MainActivity,
                    "Permission is not granted.",
                    Toast.LENGTH_SHORT
                ).show()
                checkPermission()
            }
        }
    }

    override fun onCreateLoader(id: Int, args: Bundle?): Loader<Cursor> {
        return when (id) {
            0 -> CursorLoader(
                this,
                ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                PROJECTION_NUMBERS,
                null,
                null,
                null
            )
            1 -> CursorLoader(
                this,
                ContactsContract.Contacts.CONTENT_URI,
                PROJECTION_DETAILS,
                null,
                null,
                null
            )
            else -> CursorLoader(
                this,
                ContactsContract.CommonDataKinds.Email.CONTENT_URI,
                PROJECTION_EMAIL,
                null,
                null,
                null
            )

        }
    }

    override fun onLoadFinished(loader: Loader<Cursor>, data: Cursor?) {
        when (loader.id) {
            0 -> {
                contactMap = mutableMapOf()
                if (data != null) {
                    while (!data.isClosed && data.moveToNext()) {
                        val contactId: Long? = data.getLong(0)
                        val phone: String? = data.getString(1)
                        //Log.d(TAG, "onLoadFinished: $phone")

                        var contact: Contact? = Contact(contactId)
                        if (contactMap.containsKey(contactId)) {
                            contact = contactMap[contactId]
                        } else {
                            contactMap[contactId] = contact
                        }
                        if (contact?.number == null){
                            contact?.number = mutableListOf()
                        }
                        contact?.number?.add(phone)
                    }
                    data.close()
                }
                LoaderManager.getInstance(this)
                    .initLoader(1, null, this)
            }
            1 -> if (data != null) {
                while (!data.isClosed && data.moveToNext()) {
                    val contactId: Long? = data.getLong(0)
                    val name: String? = data.getString(1)
                    val photo: String? = data.getString(2)
                    //Log.d(TAG, "onLoadFinished: $name")

                    var contact: Contact? = Contact(contactId)
                    if (contactMap.containsKey(contactId)) {
                        contact = contactMap[contactId]
                    } else {
                        contactMap[contactId] = contact
                    }
                    contact?.name = name
                    contact?.photoUri = photo
                }
                data.close()
                LoaderManager.getInstance(this)
                    .initLoader(2, null, this)
            }
            2 -> if (data != null) {
                while (!data.isClosed && data.moveToNext()) {
                    val contactId: Long? = data.getLong(0)
                    val email: String? = data.getString(1)

                    Log.d(TAG, "onLoadFinished: $email")
                    var contact: Contact? = Contact(contactId)
                    if (contactMap.containsKey(contactId)) {
                        contact = contactMap[contactId]

                    } else {
                        contactMap[contactId] = contact
                        contact?.email = mutableListOf()
                    }
                    if (contact?.email == null){
                        contact?.email = mutableListOf()
                    }
                    contact?.email?.add(email)
                }
                data.close()
                contactMap.forEach {
                    if (it.value?.name != null) {
                        viewModel.update(it.value)

                    }
                }
                val end = System.currentTimeMillis()
                val total = end - start
                Log.d(TAG, "onLoadFinished: Total $total")
            }
        }
    }

    override fun onLoaderReset(loader: Loader<Cursor>) {
    }

    private fun showAlertDialog(contact: Contact?) {

        Log.d(TAG, "showAlertDialog: ${contact?.id}")

        val factory: LayoutInflater = LayoutInflater.from(this)
        val view: View = factory.inflate(R.layout.alert_dialog, null)
        val dialog: AlertDialog = AlertDialog.Builder(this).create()
        val name = view.findViewById(R.id.tv_name) as TextView
        val number = view.findViewById(R.id.tv_number) as TextView
        val email = view.findViewById(R.id.tv_email) as TextView
        val company = view.findViewById(R.id.tv_company) as TextView
        val image = view.findViewById(R.id.image) as ImageView
        dialog.setView(view)
        dialog.setCancelable(true)
        dialog.setContentView(R.layout.alert_dialog)

        name.text = "Name: ${contact?.name}"
        number.text = "Number: \n ${Utils.getListAsString(contact?.number)}"
        email.text = "Email: \n ${Utils.getListAsString(contact?.email)}"
        company.text = if (contact?.company == null) {
            "Company: "
        } else {
            "Company: ${contact.company}"
        }

        Glide.with(image.context)
            .load(contact?.photoUri)
            .placeholder(R.drawable.ic_user)
            .into(image)

        dialog.show()
    }

    companion object {
        const val TAG = "MainActivity"
        private val PROJECTION_NUMBERS = arrayOf(
            ContactsContract.CommonDataKinds.Phone.CONTACT_ID,
            ContactsContract.CommonDataKinds.Phone.NUMBER
        )
        private val PROJECTION_DETAILS = arrayOf(
            ContactsContract.Contacts._ID,
            ContactsContract.Contacts.DISPLAY_NAME,
            ContactsContract.CommonDataKinds.Phone.PHOTO_URI
        )
        private val PROJECTION_EMAIL = arrayOf(
            ContactsContract.CommonDataKinds.Phone.CONTACT_ID,
            ContactsContract.CommonDataKinds.Email.ADDRESS
        )
    }

    private fun getRawContactId(contactId: String?): String? {
        val projection =
            arrayOf(ContactsContract.RawContacts._ID)
        val selection = ContactsContract.RawContacts.CONTACT_ID + "=?"
        val selectionArgs = arrayOf(contactId)
        val c: Cursor = contentResolver.query(
            ContactsContract.RawContacts.CONTENT_URI,
            projection,
            selection,
            selectionArgs,
            null
        )
            ?: return null
        var rawContactId = -1
        if (c.moveToFirst()) {
            rawContactId = c.getInt(c.getColumnIndex(ContactsContract.RawContacts._ID))
        }
        c.close()
        return rawContactId.toString()
    }

    private fun getCompanyName(rawContactId: String): String? {
        return try {
            val orgWhere =
                ContactsContract.Data.RAW_CONTACT_ID + " = ? AND " + ContactsContract.Data.MIMETYPE + " = ?"
            val orgWhereParams = arrayOf(
                rawContactId,
                ContactsContract.CommonDataKinds.Organization.CONTENT_ITEM_TYPE
            )
            val cursor: Cursor = contentResolver.query(
                ContactsContract.Data.CONTENT_URI,
                null, orgWhere, orgWhereParams, null
            )
                ?: return null
            var name: String? = null
            if (cursor.moveToFirst()) {
                name =
                    cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Organization.COMPANY))
            }
            cursor.close()
            name
        } catch (e: Exception) {
            e.printStackTrace()
            null
        }
    }
}