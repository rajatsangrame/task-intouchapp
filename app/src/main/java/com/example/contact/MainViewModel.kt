package com.example.contact

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.example.contact.data.Repository
import com.example.contact.data.db.Contact


/**
 * Created by Rajat Sangrame on 25/6/20.
 * http://github.com/rajatsangrame
 */
class MainViewModel(application: Application) : AndroidViewModel(application) {

    private val repository = Repository(application)
    private val queryLiveData = MutableLiveData<String>()
    private val liveDataQuery: LiveData<List<Contact?>> = Transformations.switchMap(queryLiveData) {
        repository.getLiveDataByQuery(it)
    }

    fun setLiveQuery(query: String) {
        queryLiveData.postValue(query)
    }

    fun getLiveData(): LiveData<List<Contact?>> {
        return repository.getAllContacts()
    }

    fun getLiveDataQuery(): LiveData<List<Contact?>> {
       return liveDataQuery
    }

    fun update(contact: Contact?) {
        repository.update(contact)
    }
}