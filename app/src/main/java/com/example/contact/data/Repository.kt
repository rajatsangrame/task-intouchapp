package com.example.contact.data

import android.content.Context
import android.util.Log
import androidx.lifecycle.LiveData
import com.example.contact.data.db.Contact
import com.example.contact.data.db.ContactDatabase
import java.util.concurrent.Executors

/**
 * Created by Rajat Sangrame on 25/6/20.
 * http://github.com/rajatsangrame
 */

class Repository(context: Context) {

    private val database = ContactDatabase.getDataBase(context)
    private val dao = database.contactDao()
    private val ioExecutor = Executors.newSingleThreadExecutor()

    fun insert(contact: Contact) {
        ioExecutor.execute {
            dao.insert(contact)
        }
    }

    fun bulkInsert(contactList: List<Contact>) {
        ioExecutor.execute {
            dao.insert(contactList)
        }
    }

    fun update(contact: Contact?) {
        ioExecutor.execute {
            val c: Contact? = dao.getFromId(contact?.id)
            when {
                c == null -> {
                    dao.insert(contact)
                }
                c != contact -> {
                    dao.update(contact)
                }
                else -> {
                    Log.d(TAG, "update: ignored ${contact.name}")
                }
            }
        }
    }

    fun delete(contact: Contact?) {
        ioExecutor.execute {
            dao.delete(contact)
        }
    }

    fun getAllContacts(): LiveData<List<Contact?>> =
        dao.getAllList()

    fun getLiveDataByQuery(query: String): LiveData<List<Contact?>> =
        dao.getQueryList(query)

    companion object {
        private const val TAG = "Repository"
    }

}