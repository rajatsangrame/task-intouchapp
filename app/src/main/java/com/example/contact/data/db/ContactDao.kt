package com.example.contact.data.db

import androidx.lifecycle.LiveData
import androidx.room.*

/**
 * Created by Rajat Sangrame on 25/6/20.
 * http://github.com/rajatsangrame
 */

@Dao
interface ContactDao {

    @Query("SELECT * FROM contact ORDER BY name COLLATE NOCASE ASC")
    fun getAllList(): LiveData<List<Contact?>>

    @Query("SELECT * from contact WHERE name LIKE '%' || :query || '%' OR number LIKE '%' || :query || '%' OR email LIKE '%' || :query || '%' ORDER BY name COLLATE NOCASE ASC")
    fun getQueryList(query: String): LiveData<List<Contact?>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(contact: Contact?)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(contact: List<Contact?>)

    @Delete
    fun delete(contact: Contact?)

    @Update(onConflict = OnConflictStrategy.IGNORE)
    fun update(contact: Contact?)

    @Query("SELECT  * FROM contact WHERE id = :id")
    fun getFromId(id: Long?): Contact?
}