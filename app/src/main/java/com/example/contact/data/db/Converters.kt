package com.example.contact.data.db

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.lang.reflect.Type


/**
 * Created by Rajat Sangrame on 26/6/20.
 * http://github.com/rajatsangrame
 */
class Converters {
    @TypeConverter
    fun fromString(value: String?): MutableList<String?>? {
        val listType: Type = object : TypeToken<ArrayList<String?>?>() {}.type
        return Gson().fromJson(value, listType)
    }

    @TypeConverter
    fun fromArrayList(list: MutableList<String?>?): String? {
        val gson = Gson()
        return gson.toJson(list)
    }
}