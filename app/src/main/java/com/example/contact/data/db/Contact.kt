package com.example.contact.data.db

import androidx.room.*

/**
 * Created by Rajat Sangrame on 25/6/20.
 * http://github.com/rajatsangrame
 */

@Entity(tableName = "contact")
data class Contact(
    @PrimaryKey
    val id: Long?,

    @ColumnInfo(name = "name")
    var name: String?,

    @ColumnInfo(name = "number")
    @TypeConverters(Converters::class)
    var number: MutableList<String?>?,

    @ColumnInfo(name = "photo_uri")
    var photoUri: String?,

    @ColumnInfo(name = "email")
    @TypeConverters(Converters::class)
    var email: MutableList<String?>?,

    @ColumnInfo(name = "company")
    var company: String? = null
) {
    @Ignore
    constructor(id : Long?) : this(id, null, null, null, null)
}

