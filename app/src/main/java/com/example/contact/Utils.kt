package com.example.contact

import java.lang.StringBuilder


/**
 * Created by Rajat Sangrame on 27/6/20.
 * http://github.com/rajatsangrame
 */
object Utils {

    fun getListAsString(list: MutableList<String?>?): String? {
        val sb = StringBuilder()
        if (list == null){
            return sb.toString()
        }
        list.forEach {
            sb.append(it)
            sb.append("\n")
        }
        return sb.toString()
    }
}

