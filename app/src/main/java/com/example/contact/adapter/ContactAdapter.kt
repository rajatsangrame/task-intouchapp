package com.example.contact.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.contact.R
import com.example.contact.Utils
import com.example.contact.data.db.Contact
import kotlinx.android.synthetic.main.recycler_item.view.*


/**
 * Created by Rajat Sangrame on 26/6/20.
 * http://github.com/rajatsangrame
 */
class ContactAdapter(val listener: (Contact?) -> Unit) :
    RecyclerView.Adapter<ContactAdapter.ViewHolder>() {

    private var list: List<Contact?> = ArrayList()

    fun setList(list: List<Contact?>) {
        this.list = list
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContactAdapter.ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.recycler_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ContactAdapter.ViewHolder, position: Int) {
        holder.bind(list[position])
    }

    override fun getItemCount(): Int = list.size

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view), View.OnClickListener {

        init {
            view.setOnClickListener(this)
        }

        private val tvName: TextView = view.findViewById(R.id.tv_name)
        private val tvNumber: TextView = view.findViewById(R.id.tv_number)
        private val tvEmail: TextView = view.findViewById(R.id.tv_email)
        private val imageView: ImageView = view.findViewById(R.id.image)

        fun bind(contact: Contact?) {
            tvName.text = "${contact?.name}"
            tvNumber.text = "${Utils.getListAsString(contact?.number)}"
            tvEmail.text = "${Utils.getListAsString(contact?.email)}"
            Glide.with(imageView.context)
                .load(contact?.photoUri)
                .placeholder(R.drawable.ic_user)
                .into(imageView)
        }

        override fun onClick(v: View?) {
            listener(list[adapterPosition])
        }
    }
}